package az.ingrees.demo.service;

import az.ingrees.demo.config.Config;
import az.ingrees.demo.dto.StudentDto;
import az.ingrees.demo.mapper.StudentMapper;
import az.ingrees.demo.model.Student;
import az.ingrees.demo.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class StudentServiceImp implements StudentService {
    private final StudentRepository studentRepository;
    private final Config config;
    private final StudentMapper studentMapper;

    public StudentServiceImp(StudentRepository studentRepository, Config config, StudentMapper studentMapper) {
        this.studentRepository = studentRepository;
        this.config = config;
        this.studentMapper = studentMapper;
    }

    @Override
    public StudentDto get(Integer id) {
       // System.out.println(config.getList());
        //log.info("Student service get method is working");
        Student student = studentRepository.findById(id).orElseThrow(() -> new RuntimeException("Student not found"));
        return studentMapper.entityToDto(student);
    }

    @Override
    public Student create(Student student) {
        log.info("Student service create method is working");
        Student studentInDb = studentRepository.save(student);
        return studentInDb;

    }

    @Override
    public Student update(Integer id, Student student) {
        log.info("Student service update method is working");
        Student entity = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        entity.setAge(student.getAge());
        entity.setName(student.getName());
        entity.setLastname(student.getLastname());
        entity = studentRepository.save(entity);
        return entity;
    }

    @Override
    public void delete(Integer id) {
        log.info("Student service delete method is working");
        studentRepository.deleteById(id);
    }

    @Override
    public Student test(String name, String lastname) {
        return null;
    }
}



