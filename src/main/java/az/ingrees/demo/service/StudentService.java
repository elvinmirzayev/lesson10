package az.ingrees.demo.service;

import az.ingrees.demo.dto.StudentDto;
import az.ingrees.demo.model.Student;

public interface StudentService {
    StudentDto get(Integer id);

    Student create(Student student);

    Student update(Integer id, Student student);

    void delete(Integer id);

    Student test(String name, String lastname);
}
