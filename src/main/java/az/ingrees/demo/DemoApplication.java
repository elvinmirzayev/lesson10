package az.ingrees.demo;

import az.ingrees.demo.model.Student;
import az.ingrees.demo.service.StudentService;
import az.ingrees.demo.service.StudentServiceImp;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class DemoApplication implements CommandLineRunner {

    private final  EntityManagerFactory emf;
    private final StudentService studentService;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //==========================JDBC=====================================
//            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "3169");
//            Statement statement = con.createStatement();
//            ResultSet resultSet = statement.executeQuery("select * from student");
//            while (resultSet.next()) {
//               System.out.println(resultSet.getInt(1));
//               System.out.println(resultSet.getInt(2));
//             System.out.println(resultSet.getString(3));
//              System.out.println(resultSet.getString(4));
//              System.out.println(resultSet.getString(5));
//              System.out.println(resultSet.getString(6));


//        EntityManager entityManager=emf.createEntityManager();
////        List<Student> resultList = entityManager.createNativeQuery("select * from student where age > :age", Student.class)
////                .setParameter("age",16)
////                .getResultList();
//
////        Student entity=(Student) entityManager.createNativeQuery("select * from student where age > :age",Student.class)
////                .setParameter("age",1)
////                .getResultList();
//
//        entityManager.getTransaction().begin();
//
//        int i= entityManager
//                .createNativeQuery("insert into student(name, lastname, age, phone ,middle_name) values ('Arif', 'Aliyev', 29, '994995231496', 'Davud')")
//                .executeUpdate();
//        entityManager.getTransaction().commit();
//
//
////        System.out.println(resultList);
//        List<Student> resultList1 = entityManager.createNativeQuery("select * from student where age > :age", Student.class)
//                .setParameter("age",16)
//                .getResultList();
//        entityManager.close();
//        System.out.println(resultList1);
//        System.out.println(entity);
        Student student= studentService.test("Elvin","Mirzayev");
        System.out.println(student);
    }

}
