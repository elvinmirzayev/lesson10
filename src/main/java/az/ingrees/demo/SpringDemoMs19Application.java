package az.ingrees.demo;

import az.ingrees.demo.model.Balance;
import az.ingrees.demo.model.Phone;
import az.ingrees.demo.model.Role;
import az.ingrees.demo.model.Student;
import az.ingrees.demo.repository.BalanceRepository;
import az.ingrees.demo.repository.PhoneRepository;
import az.ingrees.demo.repository.RoleRepository;
import az.ingrees.demo.repository.StudentRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.Optional;

@SpringBootApplication
@RequiredArgsConstructor
public class SpringDemoMs19Application implements CommandLineRunner {

    private final StudentRepository studentRepository;
    private final BalanceRepository balanceRepository;
    private final PhoneRepository phoneRepository;
    private final RoleRepository roleRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpringDemoMs19Application.class, args);
    }


    @Override
    @Transactional
    public void run(String... args) throws Exception {

//        Phone phone1 = Phone.builder()
//                .number("994553332211")
//                .build();
//
//        Phone phone2 = Phone.builder()
//                .number("994505544423")
//                .build();
//        Phone phone3 = Phone.builder()
//                .number("99444311111")
//                        .build();
//
//        Student student = Student.builder()
//                .name("Ali")
//                .age(15)
//                .phoneList(List.of(phone1, phone2, phone3))
//                .lastname("Valiyev")
//                .build();
//
//        phone1.setStudent(student);
//        phone2.setStudent(student);
//        phone3.setStudent(student);
//
//
//        studentRepository.save(student);
//        Student student = studentRepository.findById(1).get();
//
//        Phone build = Phone.builder()
//                .number("341361283612")
//                .student(student)
//                .build();
//
//        student.getPhoneList().add(build);
//
//        studentRepository.save(student);

//        Role user = Role.builder()
//                .role("user")
//                .build();
//
//        Role admin = Role.builder()
//                .role("admin")
//                .build();
//
//        roleRepository.save(user);
//        roleRepository.save(admin);

//        Role user = roleRepository.findByRole("user").get();
//        Role admin = roleRepository.findByRole("admin").get();
//
//        Student student = Student.builder()
//                .name("aaa")
//                .age(15)
//                .lastname("bbbbb")
//                .roles(List.of(user))
//                .build();
//        studentRepository.save(student);

        Student student = studentRepository.findById(1).get();
        studentRepository.delete(student);


//        System.out.println(student);
//        Phone phone = phoneRepository.findById(7).get();
//        System.out.println(phone);
//        System.out.println(phone.getStudent());
    }
}

