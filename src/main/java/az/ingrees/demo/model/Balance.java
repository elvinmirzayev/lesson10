package az.ingrees.demo.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Balance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    Double amount;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    Student student;


}

