package az.ingrees.demo.controller;


import az.ingrees.demo.dto.StudentDto;
import az.ingrees.demo.model.Student;
import az.ingrees.demo.service.StudentService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")

public class StudentController {
    private final StudentService studentService;
    public StudentController(StudentService studentService){
        this.studentService=studentService;
    }
    @GetMapping("/{id}")
    public StudentDto get(@PathVariable Integer id) {
        return studentService.get(id);
    }
    @PostMapping
    public Student create(@RequestBody Student student){
        return studentService.create(student);
    }

    @PutMapping("/{id}")
    public Student update(@PathVariable Integer id, @RequestBody Student student){
        return studentService.update(id,student);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        studentService.delete(id);
    }

    @GetMapping
    public Student getOzelStudent(@RequestParam String name, @RequestParam String lastname) {
        return studentService.test(name, lastname);
    }
}


