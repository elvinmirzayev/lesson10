package az.ingrees.demo.repository;

import az.ingrees.demo.model.Balance;
import az.ingrees.demo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface BalanceRepository extends JpaRepository<Balance, Integer> {

    Optional<Balance> findByStudent(Student student);

}

