package az.ingrees.demo.repository;

import az.ingrees.demo.model.Role;
import az.ingrees.demo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Optional<Role> findByRole(String role);

}

