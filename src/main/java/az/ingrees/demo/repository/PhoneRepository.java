package az.ingrees.demo.repository;

import az.ingrees.demo.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<Phone, Integer> {


}

