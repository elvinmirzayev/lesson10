package az.ingrees.demo;

public class StudentExample {


    private static StudentExample instance;

    private StudentExample() {

    }

    public static StudentExample getInstance() {
        if (instance==null) {
            synchronized (StudentExample.class) {
                if (instance == null) {
                    instance = new StudentExample();
                }
            }
        }
        return instance;
    }
}
